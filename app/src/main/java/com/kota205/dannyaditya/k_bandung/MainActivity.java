package com.kota205.dannyaditya.k_bandung;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kota205.dannyaditya.k_bandung._sliders.FragmentSlider;
import com.kota205.dannyaditya.k_bandung._sliders.SliderIndicator;
import com.kota205.dannyaditya.k_bandung._sliders.SliderPagerAdapter;
import com.kota205.dannyaditya.k_bandung._sliders.SliderView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private SliderPagerAdapter mAdapter;
    private SliderIndicator mIndicator;

    private SliderView sliderView;
    private LinearLayout mLinearLayout;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;

    private CardView nearbyDestination, myBudget, topRecommended, inputNew;

    private MenuView.ItemView itemView;

    private LocationManager locationManager;
    private LocationListener locationListener;

    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sliderView = (SliderView) findViewById(R.id.sliderView);
        mLinearLayout = (LinearLayout) findViewById(R.id.pagesContainer);
        setupSlider();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);

        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        nearbyDestination = (CardView) findViewById(R.id.nearby_destination);
        myBudget = (CardView) findViewById(R.id.my_budget);
        topRecommended = (CardView) findViewById(R.id.top_recommended_destination);
        inputNew = (CardView) findViewById(R.id.input_data_new_destination);

        nearbyDestination.setOnClickListener(this);
        myBudget.setOnClickListener(this);
        topRecommended.setOnClickListener(this);
        inputNew.setOnClickListener(this);
        

        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d("Location", location.toString());
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        if (Build.VERSION.SDK_INT < 23) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

        } else {

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.

                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            } else {

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

            }

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent i;

        if(mToggle.onOptionsItemSelected(item)) {

            return true;
        }

        switch (item.getItemId()) {
            case R.id.action_search:
                i = new Intent(this, SearchActivity.class);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    private void setupSlider() {
        sliderView.setDurationScroll(800);
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(FragmentSlider.newInstance("https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Tangkuban_Prahu.jpg/1200px-Tangkuban_Prahu.jpg"));
        fragments.add(FragmentSlider.newInstance("https://1.bp.blogspot.com/-0S2Ex-IxBqU/WauWkXxoYMI/AAAAAAAADsI/IE5ztFAJQrIw-4SU1Ubktddtf17ZiWtVQCLcBGAs/s1600/The-Lodge-Maribaya.jpg"));
        fragments.add(FragmentSlider.newInstance("http://anekatempatwisata.com/wp-content/uploads/2018/03/Farm-House-Lembang.jpg"));
        fragments.add(FragmentSlider.newInstance("http://4.bp.blogspot.com/-k6VtMOi4AHA/VGipt_JdiYI/AAAAAAAAEnA/PkHtOKi1fL0/s1600/Curug%2BMalela%2BKab.%2BBandung.jpg"));

        mAdapter = new SliderPagerAdapter(getSupportFragmentManager(), fragments);
        sliderView.setAdapter(mAdapter);
        mIndicator = new SliderIndicator(this, mLinearLayout, sliderView, R.drawable.circle_indicator);
        mIndicator.setPageCount(fragments.size());
        mIndicator.show();
    }

    @Override
    public void onClick(View v) {
        Intent i;

        switch (v.getId()) {
            case R.id.nearby_destination :
                i = new Intent(this, NearbyFragment.class);
                startActivity(i);
                break;
            case R.id.my_budget :
                i = new Intent(this, MyBudgetFragment.class);
                startActivity(i);
                break;
            case R.id.top_recommended_destination :
                i = new Intent(this, RecommendedFragment.class);
                startActivity(i);
                break;
            case R.id.input_data_new_destination :
                i = new Intent(this, InputDestination.class);
                startActivity(i);
                break;
            default:
                break;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            }
        }
    }
}