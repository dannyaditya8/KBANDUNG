package com.kota205.dannyaditya.k_bandung;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {

    private EditText searchText;
    private RecyclerView recyclerView;

    private DatabaseReference databaseReference;

    ArrayList<String> addressList;
    ArrayList<String> imagesList;
    ArrayList<String> nameList;
    ArrayList<String> opened_closedList;
    ArrayList<String> entranceticketList;
    SearchAdapter searchAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        databaseReference = FirebaseDatabase.getInstance().getReference();

        addressList = new ArrayList<>();
        imagesList = new ArrayList<>();
        nameList = new ArrayList<>();
        opened_closedList = new ArrayList<>();
        entranceticketList = new ArrayList<>();

        searchText = (EditText) findViewById(R.id.search_edit);
        recyclerView = (RecyclerView) findViewById(R.id.result_list);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    setAdapter(s.toString());
                } else {

                    nameList.clear();
                    addressList.clear();
                    imagesList.clear();
                    opened_closedList.clear();
                    entranceticketList.clear();
                    recyclerView.removeAllViews();

                }
            }
        });

    }

    private void setAdapter(final String searchedString) {

        databaseReference.child("tourist_attraction_park").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                nameList.clear();
                addressList.clear();
                imagesList.clear();
                opened_closedList.clear();
                entranceticketList.clear();
                recyclerView.removeAllViews();
                int counter = 0;

                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    String address = snapshot.child("address").getValue(String.class);
                    String image = snapshot.child("image").getValue(String.class);
                    String name = snapshot.child("name").getValue(String.class);
                    String opened_closed = snapshot.child("opened_closed").child("opened_closed").getValue(String.class);
                    String entrance_ticket = snapshot.child("opened_closed").child("entrance_ticket").getValue(String.class);

                    if (name.toLowerCase().contains(searchedString.toLowerCase())) {
                        addressList.add(address);
                        imagesList.add(image);
                        nameList.add(name);
                        opened_closedList.add(opened_closed);
                        entranceticketList.add(entrance_ticket);
                        counter++;
                    } else if (address.toLowerCase().contains(searchedString.toLowerCase())){
                        addressList.add(address);
                        imagesList.add(image);
                        nameList.add(name);
                        opened_closedList.add(opened_closed);
                        entranceticketList.add(entrance_ticket);
                        counter++;
                    }

                    if (counter == 15) {
                        break;
                    }
                }

                searchAdapter = new SearchAdapter(SearchActivity.this, addressList, imagesList, nameList, opened_closedList, entranceticketList);
                recyclerView.setAdapter(searchAdapter);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
