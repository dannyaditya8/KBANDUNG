package com.kota205.dannyaditya.k_bandung;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.renderscript.ScriptGroup;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class InputDestination extends AppCompatActivity {

    private ImageButton imageButton;
    private EditText namaWisata;
    private EditText alamatWisata;
    private EditText fasilitasWisata;
    private EditText alasan;
    private EditText status;

    private Button submitBtn;

    private Uri imageUri = null;

    private StorageReference storageReference;
    private DatabaseReference databaseReference;

    private ProgressDialog progressDialog;
    private static final int GALLERY_REQUEST = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_destination);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imageButton = (ImageButton) findViewById(R.id.img_btn);
        namaWisata = (EditText) findViewById(R.id.nama_wisata);
        alamatWisata = (EditText) findViewById(R.id.alamat_wisata);
        fasilitasWisata = (EditText) findViewById(R.id.fasilitas_wisata);
        alasan = (EditText) findViewById(R.id.alasan);
        status = (EditText) findViewById(R.id.status);
        submitBtn = (Button) findViewById(R.id.submit_btn);

        progressDialog = new ProgressDialog(this);

//        String content = status.getText().toString();
//
//        Toast.makeText(InputDestination.this, "Status :"+content, Toast.LENGTH_LONG).show();

        storageReference = FirebaseStorage.getInstance().getReference();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("temp");

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, GALLERY_REQUEST);
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startSubmit();

            }
        });
    }

    private void startSubmit() {

        progressDialog.setMessage("Saving data to database");
        progressDialog.show();

        final String nama_value = namaWisata.getText().toString().trim();
        final String alamat_value = alamatWisata.getText().toString().trim();
        final String fasilitas_value = fasilitasWisata.getText().toString().trim();
        final String alasan_value = alasan.getText().toString().trim();
        final String status_value = status.getText().toString().trim();

        if (!TextUtils.isEmpty(nama_value) && !TextUtils.isEmpty(alamat_value)
                && !TextUtils.isEmpty(fasilitas_value)
                && !TextUtils.isEmpty(alasan_value)
                && !TextUtils.isEmpty(status_value)
                && imageUri != null) {

            StorageReference filePath = storageReference.child("temp_images").child(imageUri.getLastPathSegment());

            filePath.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    Task<Uri> downloadUrl = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                    DatabaseReference newTempData = databaseReference.push();
                    newTempData.child("nama_wisata").setValue(nama_value);
                    newTempData.child("alamat_wisata").setValue(alamat_value);
                    newTempData.child("fasilitas_wisata").setValue(fasilitas_value);
                    newTempData.child("alasan_user").setValue(alasan_value);
                    newTempData.child("status_validasi").setValue(status_value);
                    newTempData.child("gambar_wisata").setValue(downloadUrl.toString());

                    progressDialog.dismiss();
                    namaWisata.setText("");
                    alamatWisata.setText("");
                    fasilitasWisata.setText("");
                    alasan.setText("");
                    imageUri = null;
                    imageButton.setImageURI(imageUri);
                    imageButton.setImageResource(R.mipmap.add_btn);
                    Toast.makeText(InputDestination.this, "Data sudah masuk kedalam database", Toast.LENGTH_LONG).show();

                }
            });

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {

            imageUri = data.getData();
            imageButton.setImageURI(imageUri);
        }
    }
}
