package com.kota205.dannyaditya.k_bandung;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class TouristAttractionDetail extends AppCompatActivity {

    private static final String TAG = "TouristAttractionDetail";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tourist_attraction_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        getIncomingIntent();

    }

    private void getIncomingIntent(){
        if (getIntent().hasExtra("image_url") && getIntent().hasExtra("name")) {

            String imageUrl = getIntent().getStringExtra("image_url");
            String nameDetail = getIntent().getStringExtra("name");
            getSupportActionBar().setTitle(nameDetail);
            setImage(imageUrl, nameDetail);
        }
    }

    private void setImage(String imageUrl, String nameDetail) {
        TextView setName = (TextView) findViewById(R.id.text_detail);
        setName.setText(nameDetail);

        ImageView image = (ImageView) findViewById(R.id.image_detail);
        Glide.with(this).load(imageUrl).asBitmap().into(image);

    }
}
