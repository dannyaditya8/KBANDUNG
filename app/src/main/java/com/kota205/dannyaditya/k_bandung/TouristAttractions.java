package com.kota205.dannyaditya.k_bandung;

public class TouristAttractions {

    public String name, address, images, opened_closed, entrance_ticket;

    public TouristAttractions() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getOpened_closed() {
        return opened_closed;
    }

    public void setOpened_closed(String opened_closed) {
        this.opened_closed = opened_closed;
    }

    public String getEntrance_ticket() {
        return entrance_ticket;
    }

    public void setEntrance_ticket(String entrance_ticket) {
        this.entrance_ticket = entrance_ticket;
    }
}
