package com.kota205.dannyaditya.k_bandung;

import android.view.View;

public interface ItemClickListener {

    void onClick(View view, int position, boolean isLongClick);
}
