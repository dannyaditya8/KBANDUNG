package com.kota205.dannyaditya.k_bandung;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {
    Context context;
    ArrayList<String> addressList;
    ArrayList<String> imagesList;
    ArrayList<String> nameList;
    ArrayList<String> entranceTicketList;
    ArrayList<String> opened_closedList;

    public static class SearchViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

        ImageView imagestouristAttraction;
        TextView nametouristAttraction, addresstouristAttraction, openedclosedTouristAttraction, entranceticketTouristAttraction;
        ItemClickListener clickListener;


        public SearchViewHolder(View itemView) {
            super(itemView);
            imagestouristAttraction = (ImageView) itemView.findViewById(R.id.images_touristAttraction);
            nametouristAttraction = (TextView) itemView.findViewById(R.id.name_touristAttraction);
            addresstouristAttraction = (TextView) itemView.findViewById(R.id.address_touristAttraction);
            openedclosedTouristAttraction = (TextView) itemView.findViewById(R.id.opened_closedTouristAttraction);
            entranceticketTouristAttraction = (TextView) itemView.findViewById(R.id.entranceticket_touristAttraction);
            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void setClickListener(ItemClickListener itemClickListener){
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            clickListener.onClick(v, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onClick(v, getAdapterPosition(), true);
            return true;
        }
    }

    public SearchAdapter(Context context, ArrayList<String> addressList, ArrayList<String> imagesList, ArrayList<String> nameList, ArrayList<String> opened_closedList, ArrayList<String> entranceTicketList) {
        this.context = context;
        this.addressList = addressList;
        this.imagesList = imagesList;
        this.nameList = nameList;
        this.opened_closedList = opened_closedList;
        this.entranceTicketList = entranceTicketList;
    }

    @Override
    public SearchAdapter.SearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.activity_search_list_items, parent, false);
        return new SearchAdapter.SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchViewHolder holder, int position) {
        Glide.with(context).load(imagesList.get(position)).asBitmap().placeholder(R.drawable.no_image).into(holder.imagestouristAttraction);
        holder.addresstouristAttraction.setText(addressList.get(position));
        holder.nametouristAttraction.setText(nameList.get(position));
        holder.openedclosedTouristAttraction.setText(opened_closedList.get(position));
        holder.entranceticketTouristAttraction.setText("Harga Tiket : "+entranceTicketList.get(position));

        holder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {

                Intent intent = new Intent(context, TouristAttractionDetail.class);
                intent.putExtra("image_url", imagesList.get(position));
                intent.putExtra("name", nameList.get(position));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return addressList.size();
    }
}
